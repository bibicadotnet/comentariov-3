# Comentario

[Homepage](https://comentario.app/) · [Demo](https://demo.comentario.app) · [Docs](https://docs.comentario.app/) · [Author's Blog](https://yktoo.com/)

---

**[Comentario](https://comentario.app)** is a platform that you can embed in your website to allow your readers to add comments. It's lightweight and fast.

Comentario supports Markdown syntax, import from Disqus, comment voting, automated spam and toxicity detection, moderation tools, sticky comments, thread locking, OAuth login, email notifications, user roles, single sign-on, and more.

**Comentario** 2.x was a fork of [Commento](https://gitlab.com/commento/commento) by Adhityaa Chandrasekar, now discontinued.

As of **Comentario 3**, the whole codebase and the data model have been rewritten, so any resemblance to Commento is mostly cosmetic.

## FAQ

### How is this different from Disqus, Facebook Comments, and the rest?

Most other products in this category have privacy downsides you should know about.

Many of these systems like to collect data — stuff like analytics, ads, and user info. This can be a privacy issue, especially if they're after your personal data.

Apart from turning you, the user, into a product, theres a data security concern. If the commenting system gets hacked, your info might end up in the wrong hands. A good idea is to check the privacy policies and how they handle data before picking a commenting system for your site.

Here's a start:

* Comentario has no ads: you're the customer, not the product.
* Comentario offers self-hosted options that give you more control over privacy settings.
* Comentario is orders of magnitude lighter than alternatives: the downloadable embed part is some 20 KB compressed.

### Why should I care about my readers' privacy?

Firstly, it fosters trust and bolsters your website's reputation. When users believe that their personal information is handled responsibly, they are more likely to engage with your content and feel comfortable interacting with your online platform. Additionally, compliance with privacy laws and regulations is a must, as failure to do so can lead to legal repercussions and fines, making it essential to prioritize privacy as a fundamental aspect of your online presence.

Moreover, beyond legal requirements, there are ethical considerations. Respecting user privacy reflects a commitment to treating your readers with respect and acknowledging the value of their personal data. This ethical stance not only aligns with responsible online practices but also contributes to long-term sustainability by fostering user loyalty and trust. By prioritizing user privacy, you create a secure and welcoming digital space, which is vital for your website's credibility, user engagement, and overall success.

### How does Comentario differ from its predecessor Commento?

The list of differences is [really long](CHANGELOG.md), but here are a few major points:

* Comentario is in active development, regularly adding tons of features and improvements.
* Comentario is running the latest and greatest software versions, with all necessary security updates.
* Comentario is blazing fast due to extensive code and data model optimisations.
* Comentario is built following the best practices for security, privacy, responsive design, and accessibility.
* Comentario is (aiming to be) fully tested using automated tests to prevent regressions.

## Getting started

Refer to [Comentario documentation](https://docs.comentario.app/en/getting-started/) to learn how to install and configure it.
