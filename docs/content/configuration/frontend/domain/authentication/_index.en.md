---
title: Authentication
description: Authentication domain settings
weight: 20
tags:
    - configuration
    - frontend
    - Administration UI
    - domain
    - authentication
    - local authentication
    - anonymous
    - Facebook
    - GitHub
    - GitLab
    - Google
    - Twitter
    - SSO
    - Single Sign-On
seeAlso:
    - /configuration/frontend/domain
---

The `Authentication` tab allows to configure commenter authentication options for the domain.

<!--more-->

These options include:

* Anonymous comments
* Local (username-and-password-based)
* Social login via external identity providers (Facebook, GitHub, GitLab, Google, Twitter)
* [Single Sign-On](sso)
